# Occurrences Manager Api (example of usage of Django Rest framework)


#### API Rest that enables the possibility of managing an urban area occurrences.

    #Build docker container
	docker-compose build
	
	#Up docker, first db then api
	docker-compose up db
	docker-compose up api
	
	#Run the migrations
	docker-compose exec api python manage.py migrate
	
	#Load initial data (base occurrence categories)
	docker-compose exec api python manage.py loaddata initial
	
	#Create first admin user
	docker-compose exec api python manage.py initadmin
	

#### Endpoints:
	/occurrences/ (list, create, update, delete)
	/authors/ (list, details)
	/create_author/ (create)

[Occurrences Postman Collection](https://gitlab.com/manuelseromenho/occurrences_manager_api/-/blob/474b46694a1cb5379d00b0c4f25c6485de111f29/Occurrences.postman_collection.json)
	
